package tk.zeitheron.wearsfr;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import tk.zeitheron.solarflux.api.SolarInfo;
import tk.zeitheron.solarflux.block.BlockBaseSolar;

public class SolarPaneInfo
{
	public static final String SPTAG = "WearSFRPanel";

	public final SolarInfo solar;

	public SolarPaneInfo(SolarInfo block)
	{
		this.solar = block;
	}

	public ResourceLocation getTopTex()
	{
		return solar.getTexture();
	}

	public static int getGeneration(ItemStack head, EntityPlayer player)
	{
		SolarPaneInfo inf = fromHelmet(head);
		if(inf != null && player.world.canSeeSky(player.getPosition()))
			return inf.getCurrentGenerationForPlayer(player);
		return 0;
	}

	public long getMaxGen()
	{
		return solar.getGeneration();
	}

	public int getCurrentGenerationForPlayer(EntityPlayer player)
	{
		float ints = computeSunIntensity(player.world, player.getPosition());
		return Math.round(ints * getMaxGen());
	}

	public static SolarPaneInfo fromHelmet(ItemStack head)
	{
		boolean panel = !head.isEmpty() && head.hasTagCompound() && head.getTagCompound().hasKey(SPTAG, NBT.TAG_STRING);
		NBTTagCompound nbt = head.getTagCompound();
		if(panel && head.getItem() instanceof ItemArmor && ((ItemArmor) head.getItem()).armorType == EntityEquipmentSlot.HEAD)
			return from(nbt);
		return null;
	}

	public static SolarPaneInfo from(Block sp)
	{
		if(sp != null && sp instanceof BlockBaseSolar)
		{
			Item item = Item.getItemFromBlock(sp);
			if(!WearableSolars.INFOS.containsKey(item))
			{
				WearableSolars.LOG.info("Assigned solar panel with it's info: " + sp.getRegistryName());
				WearableSolars.INFOS.put(item, new SolarPaneInfo(((BlockBaseSolar) sp).solarInfo));
			}
			return WearableSolars.INFOS.get(item);
		}
		return null;
	}

	public static SolarPaneInfo from(Item item)
	{
		return from(Block.getBlockFromItem(item));
	}

	public static SolarPaneInfo from(NBTTagCompound nbt)
	{
		return nbt == null || !nbt.hasKey(SPTAG, NBT.TAG_STRING) ? null : from(ForgeRegistries.ITEMS.getValue(new ResourceLocation(nbt.getString(SPTAG))));
	}

	public static float computeSunIntensity(World world, BlockPos at)
	{
		float intens = 0;

		if(world.canBlockSeeSky(at))
		{
			float mult = 1.5F;
			float displ = 1.2F;
			float celestialAngleRadians = world.getCelestialAngleRadians(1F);
			if(celestialAngleRadians > Math.PI)
				celestialAngleRadians = (float) (2 * Math.PI - celestialAngleRadians);
			intens = mult * MathHelper.cos(celestialAngleRadians / displ);
			intens = Math.max(0, intens);
			intens = Math.min(1, intens);
//			if(intens > 0)
//			{
//				if(world.isRaining())
//					intens *= ModConfiguration.getRainGenerationFactor();
//				if(world.isThundering())
//					intens *= ModConfiguration.getThunderGenerationFactor();
//			}
		}

		return intens;
	}
}