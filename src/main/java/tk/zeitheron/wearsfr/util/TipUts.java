package tk.zeitheron.wearsfr.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.zeitheron.hammercore.tile.tooltip.own.IRenderableInfo;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;

public class TipUts
{
	public static void join(ITooltip tooltip, char bet, Collection<IRenderableInfo> infos)
	{
		infos.removeIf(i -> i == null);
		List<IRenderableInfo> is = new ArrayList<>(infos);
		for(int i = 0; i < is.size(); ++i)
		{
			tooltip.append(is.get(i));
			if(i != is.size() - 1)
				tooltip.append(new StringTooltipInfo(" " + bet + " "));
		}
	}
	
	public static void join(ITooltip tooltip, char bet, IRenderableInfo... infos)
	{
		List<IRenderableInfo> is = new ArrayList<>();
		for(IRenderableInfo i : infos)
			if(i != null)
				is.add(i);
		join(tooltip, bet, is);
	}
}