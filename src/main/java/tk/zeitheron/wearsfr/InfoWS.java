package tk.zeitheron.wearsfr;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InfoWS
{
	public static final String //
	MOD_ID = "wearsfr", //
	        MOD_NAME = "Solar Flux Additions", //
	        MOD_VERSION = "@VERSION@";
	
	public static ItemStack removeSubTag(ItemStack stack, String tag)
	{
		if(stack.hasTagCompound() && stack.getTagCompound().hasKey(tag))
		{
			stack.getTagCompound().removeTag(tag);
			if(getTraversiveTagCount(stack.getTagCompound()) == 0)
				stack.setTagCompound(null);
		}
		return stack;
	}
	
	public static int getTraversiveTagCount(NBTBase nbt)
	{
		if(nbt instanceof NBTTagCompound)
		{
			int co = 0;
			for(String key : ((NBTTagCompound) nbt).getKeySet())
				co += getTraversiveTagCount(((NBTTagCompound) nbt).getTag(key));
			return co;
		} else if(nbt instanceof NBTTagList)
		{
			int size = ((NBTTagList) nbt).tagCount();
			int co = 0;
			for(int i = 0; i < size; ++i)
				co += getTraversiveTagCount(((NBTTagList) nbt).get(i));
			return co;
		}
		return nbt != null ? 1 : 0;
	}
	
	public static String info(String s)
	{
		return "info." + MOD_ID + ":" + s;
	}
}