package tk.zeitheron.wearsfr.blocks;

import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockHorizontal;
import com.zeitheron.hammercore.utils.WorldUtil;
import tk.zeitheron.wearsfr.SolarPaneInfo;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSolarInjector extends BlockDeviceHC<TileSolarInjector> implements IBlockHorizontal
{
	public BlockSolarInjector()
	{
		super(Material.IRON, TileSolarInjector.class, "solar_injector");
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileSolarInjector t = WorldUtil.cast(worldIn.getTileEntity(pos), TileSolarInjector.class);
		if(t != null)
		{
			ItemStack held = playerIn.getHeldItem(hand);
			
			if(!held.isEmpty())
			{
				if(held.getItem() instanceof ItemArmor && ((ItemArmor) held.getItem()).armorType == EntityEquipmentSlot.HEAD && SolarPaneInfo.fromHelmet(held) == null)
				{
					int s = 0;
					ItemStack stack = t.inv.getStackInSlot(s);
					if(stack.isEmpty())
					{
						t.inv.setInventorySlotContents(s, held.copy());
						t.inv.getStackInSlot(s).setCount(1);
						t.sendChangesToNearby();
						held.shrink(1);
					}
				} else if(SolarPaneInfo.from(held.getItem()) != null)
				{
					int s = 1;
					ItemStack stack = t.inv.getStackInSlot(s);
					if(stack.isEmpty())
					{
						t.inv.setInventorySlotContents(s, held.copy());
						t.inv.getStackInSlot(s).setCount(1);
						t.sendChangesToNearby();
						held.shrink(1);
					}
				}
			}
		}
		return true;
	}
}