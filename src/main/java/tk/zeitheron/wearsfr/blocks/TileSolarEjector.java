package tk.zeitheron.wearsfr.blocks;

import java.util.Arrays;

import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;
import tk.zeitheron.wearsfr.InfoWS;
import tk.zeitheron.wearsfr.SolarPaneInfo;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

public class TileSolarEjector extends TileSyncableTickable implements IEnergyStorage, ITileDroppable, ITooltipProviderHC
{
	public final InventoryDummy inv = new InventoryDummy(1);
	public final FEEnergyStorage fe = new FEEnergyStorage(4000);
	
	public int prevProgress = 0;
	public int progress = 0;
	
	public int getCraftTime()
	{
		return 10;
	}
	
	@Override
	public void tick()
	{
		prevProgress = progress;
		if(ticksExisted % 8 == 0)
			setTooltipDirty(true);
		if(canEject())
		{
			if(fe.getEnergyStored() >= 100 && atTickRate(20) && fe.extractEnergy(200, false) >= 100)
			{
				progress++;
				setTooltipDirty(true);
				sendChangesToNearby();
			}
			if(!world.isRemote && progress >= getCraftTime())
			{
				progress -= getCraftTime();
				eject();
				sendChangesToNearby();
			}
		} else if(progress > 0)
			--progress;
	}
	
	public boolean canEject()
	{
		return SolarPaneInfo.fromHelmet(inv.getStackInSlot(0)) != null;
	}
	
	public void eject()
	{
		SolarPaneInfo spi = SolarPaneInfo.fromHelmet(inv.getStackInSlot(0));
		if(spi != null)
		{
			ItemStack helm = InfoWS.removeSubTag(inv.getStackInSlot(0).copy(), SolarPaneInfo.SPTAG);
			helm.setCount(1);
			inv.getStackInSlot(0).shrink(1);
			ItemStack sps = new ItemStack(spi.solar.getBlock());
			
			EnumFacing face = WorldUtil.getFacing(getLocation().getState());
			Vec3d nv = new Vec3d(pos).add(.5 + face.getXOffset() * .65, .5, .5 + face.getZOffset() * .65);
			
			if(!world.isRemote)
			{
				EntityItem ei = new EntityItem(world, nv.x, nv.y, nv.z, helm);
				ei.motionY = 0;
				ei.motionX = face.getXOffset() * .1;
				ei.motionZ = face.getZOffset() * .1;
				world.spawnEntity(ei);
			}
			
			if(!world.isRemote)
			{
				EntityItem ei = new EntityItem(world, nv.x, nv.y, nv.z, sps);
				ei.motionY = 0;
				ei.motionX = face.getXOffset() * .1;
				ei.motionZ = face.getZOffset() * .1;
				world.spawnEntity(ei);
			}
		}
	}
	
	private void randomizeTraj(EntityItem ei)
	{
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		inv.drop(world, pos);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		prevProgress = nbt.getInteger("PrevProgress");
		inv.readFromNBT(nbt.getCompoundTag("Items"));
		progress = nbt.getInteger("Progress");
		fe.readFromNBT(nbt);
		setTooltipDirty(true);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", inv.writeToNBT(new NBTTagCompound()));
		nbt.setInteger("PrevProgress", prevProgress);
		nbt.setInteger("Progress", progress);
		fe.writeToNBT(nbt);
	}
	
	@Override
	protected IItemHandler createSidedHandler(EnumFacing side)
	{
		return itemHandlers[side.ordinal()] = new InvWrapper(inv);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return fe.receiveEnergy(maxReceive, simulate);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return fe.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return fe.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	public boolean dirty = false;
	
	@Override
	public boolean isTooltipDirty()
	{
		return dirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		this.dirty = dirty;
	}
	
	@Override
	public void addInformation(ITooltip tip)
	{
		SolarPaneInfo spi = SolarPaneInfo.fromHelmet(inv.getStackInSlot(0));
		if(spi != null)
		{
			ItemStack helm = InfoWS.removeSubTag(inv.getStackInSlot(0).copy(), SolarPaneInfo.SPTAG);
			helm.setCount(1);
			ItemStack sps = new ItemStack(spi.solar.getBlock());
			
			tip.append(new ItemStackTooltipInfo(inv.getStackInSlot(0), 16, 16));
			tip.append(new StringTooltipInfo(TextFormatting.BOLD + " \u2192 "));
			tip.append(new ItemStackTooltipInfo(helm, 16, 16));
			tip.append(new StringTooltipInfo(" & "));
			tip.append(new ItemStackTooltipInfo(sps, 16, 16));
		}
		
		tip.newLine();
		
		tip.append(new TranslationTooltipInfo(InfoWS.info("progress")));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(Math.round(progress / (float) getCraftTime() * 100) + "%"));
		
		tip.newLine();
		boolean pro = !inv.getStackInSlot(0).isEmpty();
		tip.append(new TranslationTooltipInfo(InfoWS.info(!pro ? "required.helmet.charge" : "processing")));
		if(pro)
		{
			char[] chs = new char[(ticksExisted / 8) % 4];
			Arrays.fill(chs, '.');
			tip.append(new StringTooltipInfo(new String(chs)));
		}
	}
}